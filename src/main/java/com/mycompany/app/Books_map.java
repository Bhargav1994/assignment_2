package Hash_map;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
class Books {
	String name;
	int page_numb;

	public Books(String name, int page_numb) {
		this.name = name;
		this.page_numb = page_numb;
	}

	public String put_name() {
		return this.name;
	}

	public int page_numb() {
		return this.page_numb;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Books) {
			Books c = (Books) o;
			if ((this.name == c.name) && (this.page_numb == c.page_numb))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.page_numb;
	}

	@Override
	public String toString() {
		return "("+this.put_name()+")"+"\t"+this.page_numb();
	}
	
}

public class Books_map {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Books b1 = new Books("Dracula", 504);
		Books b2 = new Books("Dracula", 504);
		Books b3 = new Books("Lord of the Rings", 90);

	
		
		@SuppressWarnings("rawtypes")
		HashMap book = new HashMap();
		System.out.println("Both objects are " +(b1.equals(b2)));
	
		
		
		book.put(1, b1);
		book.put(2, b2);
		book.put(3, b3);
		
		
		@SuppressWarnings("rawtypes")
		Set entrySet = book.entrySet(); // Set used to add the hashcode values
										// since the values are different its display same value of object
		
		System.out.println("\nUsing iterator method\n");
		@SuppressWarnings("rawtypes")
		Iterator it = entrySet.iterator();
		
		while (it.hasNext()) {
			Object element = it.next();
			System.out.println("Value: " + element);
		}

	}
}
